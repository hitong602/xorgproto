#ifndef _DEBUGPROTO_H_
#define _DEBUGPROTO_H_

#include <X11/extensions/debugconst.h>

/*********************************************************
 *
 * Protocol request constants
 *　opcode from xml
 */
#define Window CARD32
#define Context CARD32


#define X_DebugQueryVersion        0   
#define X_DebugCreateContext       1
#define X_DebugFreeContext         2
#define X_DebugRegisterClient      3   
#define X_DebugUnregisterClient    4   
#define X_DebugProbeContext        5   



/*
 * Initialize,  head/payload/tail, 整体还是 来自于xml 自动产生的
 */
typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    CARD16      majorVersion;
    CARD16      minorVersion;
} xDebugQueryVersionReq;
#define sz_xDebugQueryVersionReq 	8

typedef struct
{
    CARD8   type;
    CARD8   pad0;
    CARD16  sequenceNumber;
    CARD32  length;
    CARD16  majorVersion;
    CARD16  minorVersion;
    CARD32  pad1;
    CARD32  pad2;
    CARD32  pad3;
    CARD32  pad4;
    CARD32  pad5;
 } xDebugQueryVersionReply;
#define sz_xDebugQueryVersionReply  	32


typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    Context context;
} xDebugCreateContextReq;
#define sz_xDebugCreateContextReq 	8


typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    Context context;
} xDebugFreeContextReq;
#define sz_xDebugFreeContextReq 	8


typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    Context context;
    Window      window;
} xDebugRegisterClientReq;
#define sz_xDebugRegisterClientReq 	12

typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    Context context;
    Window      window;
} xDebugUnregisterClientReq;
#define sz_xDebugUnregisterClientReq 	12

typedef struct {
    CARD8       reqType;
    CARD8       debugReqType;
    CARD16      length;
    Context context;
} xDebugProbeContextReq;
#define sz_xDebugProbeContextReq 	8

typedef struct
{
    CARD8   type;
    CARD8   category;
    CARD16  sequenceNumber;
    CARD32  length;
    CARD32  server_time;
    CARD32		pad1;
    CARD32		pad2;
    CARD32		pad3;
    CARD32		pad4;
    CARD32		pad5;
} xDebugProbeContextReply;
#define sz_xDebugProbeContextReply  	32



#undef Window
#undef Context
#endif
