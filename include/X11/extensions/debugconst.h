#ifndef _DEBUGCONST_H_
#define _DEBUGCONST_H_

#define DEBUG_NAME			"DEBUG"
#define DEBUG_MAJOR_VERSION		0
#define DEBUG_MINOR_VERSION		1
#define DEBUG_LOWEST_MAJOR_VERSION	0
#define DEBUG_LOWEST_MINOR_VERSION	0

#define DebugNumErrors       0L
#define DebugNumEvents		0L

#define	XDebugFromServerTime		0x01
#define	XDebugFromClientTime		0x02
#define	XDebugFromClientSequence	0x04


#define XDebugFromServer           	0
#define XDebugFromClient               1
#define XDebugClientStarted           	2
#define XDebugClientDied               3
#define XDebugStartOfData		4
#define XDebugEndOfData		5


#endif /* _DEBUG_H_ */
